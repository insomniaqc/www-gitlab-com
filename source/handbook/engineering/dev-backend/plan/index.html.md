---
layout: markdown_page
title: "Plan Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Plan Team
{: #plan}

The Plan team works on the backend part of GitLab for the the [Plan product
category]. Among other things, this means working on GitLab's issue tracker,
integration with external trackers, [Markdown rendering], [Elasticsearch
integration], and email notifications.

[Plan product category]: /handbook/product/categories/#dev
[Markdown rendering]: https://docs.gitlab.com/ee/user/markdown.html
[Elasticsearch integration]: https://docs.gitlab.com/ee/integration/elasticsearch.html

### Workflow

#### The due-22nd Label

The Plan team follows the [engineering workflow], with one experimental
addition: the [due-22nd label]. This indicates issues that have a soft due-date
of the 22nd of the month (two weeks after the kick-off on the 8th), as opposed
to other issues which can be completed at any time during the month. We are
still evaluating this label and its usage, but we hope to:

1. Help Product communicate priority outside boards, in a more granular way than
   the existing Deliverable and Stretch labels.
2. Force ourselves to ensure issues are scoped down appropriately.
3. Give everyone a clearer sense of the state of the milestone; if all of the
   due-22nd issues are still open on the 24th, for instance, then we are
   probably at risk of other issues slipping.

[engineering workflow]: /handbook/engineering/workflow/
[due-22nd label]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Plan&label_name[]=due-22nd

#### Retrospectives

The Plan team conducts [monthly retrospectives in GitLab issues]. These include
the backend team, plus any people from frontend, UX, and PM who have worked with
that team during the release being retrospected.

These are confidential during the initial discussion, then made public in time
for each month's [GitLab retrospective]. For more information, see [team
retrospectives].

[monthly retrospectives in GitLab issues]: https://gitlab.com/gl-retrospectives/plan/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=retrospective
[GitLab retrospective]: /handbook/engineering/workflow/#retrospective
[team retrospectives]: /handbook/engineering/management/team-retrospectives/
