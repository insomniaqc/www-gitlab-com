---
layout: markdown_page
title: 2FA Removal
category: Support Workflows
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

Use this when a request to disable 2FA on GitLab.com is received. 

## Notes: 

[Two-factor Authentication](http://docs.gitlab.com/ee/profile/two_factor_authentication.html) (2FA)
can only be removed from a GitLab.com account under the following circumstances:

### User has a valid SSH key:

Users can generate new recovery codes using SSH, if they've previously added
SSH public keys to their profile. The new recovery codes can then be used at
sign in. This option is presented to users in the Zendesk macro. If they cannot
use this method then move on to the manual methods below.

```
ssh git@gitlab.com 2fa_recovery_codes
```

### User has recovery codes
Users can try and login using their saved [two-factor recovery codes](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#recovery-codes).

### User can provide evidence of account ownership:
If a user has lost their account recovery codes and has no SSH key registered, proving they
own the account can be difficult. In these cases, please use the [Risk Factor Worksheet](https://docs.google.com/spreadsheets/d/1NBH1xaZQSwdQdJSbqvwm1DInHeVD8_b2L08-V1QG1Qk/edit#gid=0) <internal only>.

______________

**Note: as of Aug 2018 GitLab is no longer accepting government issued ID as proof of account ownership**

______________

##### Workflow

1. Apply the **"Account::2FA Removal Verification - GitLab.com"** Macro
2. Mark the ticket as "Pending"
 
##### If the user is unable to remove 2FA using the above methods and responds with the need for further verification

1. Verify the originating email is the same as is on the account.

1. With a manager or Senior Support Agent, determine the data classification level for the account in question.
1. Select an apporpriate number of challenges from the [Risk Factor Worksheet](https://docs.google.com/spreadsheets/d/1NBH1xaZQSwdQdJSbqvwm1DInHeVD8_b2L08-V1QG1Qk/edit#gid=0) <internal only>.


1. Have the user go through the selected challenges and fill the worksheet to assess the risk

1. If the user is able to pass all challenges:
   1. As an internal note, list the data classification and risk score
   1. Disable 2FA: log into your admin account and locate the username in the users table or by going to `https://gitlab.com/admin/users/usernamegoeshere`
   1. Under the account tab, disable 2FA.
   1. Mark the ticket as "Solved" 

1. If the user is unable to pass the selected challenges:
   1. Inform them that without verification we will not be able to reset 2FA.
   1. Mark the ticket as "Solved"


##### User responds with repository verification

1. Verify the file uploaded
    + File contains the provided text string.
    + File has been uploaded to a "Personal Repository"

2. Apply an "Internal Comment" with a link to the commit (if not already included)
3. Apply the ID matches, use the **Account::2FA Removal Verification - GitLab.com - Successful** Macro

##### Failed to verify 

1. Apply the **Account::2FA Removal Verification - GitLab.com - Failed** Macro

__________________

**Macros**

* [Account::2FA Removal Verification - GitLab.com](https://gitlab.zendesk.com/agent/admin/macros/103721068)
* [Account::2FA Removal Verification - GitLab.com - Failed](https://gitlab.zendesk.com/agent/admin/macros/103790308)
* [Account::2FA Removal Verification - GitLab.com - Successful](https://gitlab.zendesk.com/agent/admin/macros/103772548)

## GitLab Team Members 2FA Removal

If the user is a GitLab employee, follow the below process:

1. Perform steps for SSH key and recovery codes, if possible.

2. Confirm authenticity of the request by contacting the employee via phone or video call.

