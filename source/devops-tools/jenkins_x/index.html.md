---
layout: markdown_page
title: "Jenkins X"
---
<!-- This is the template for sections to include and the order to include
them. If a section has no content yet then leave it out. Leave this note in
tact so that others can see where new sections should be added.

### Summary
### Strengths
### Challenges
### Who buys and why
### Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
### Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
### FAQs
 - about the product  <-- comment. delete this line
### Integrations
### Pricing
   - summary, links to tool website  <-- comment. delete this line
### Comparison
   - link to comparison page  <-- comment. delete this line
### Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

### Summary
 Jenkins X natively integrates Jenkins CI/CD server, Kubernetes, Helm, and other tools to offer a prescriptive CI/CD pipeline with best practices built-in, such as using GitOps to manage environments. It uses deployment of Jenkins into Kubernetes containers to get around the complexities of installing and integrating Jenkins. However, it is a complex pairing of many tools including the fragile Jenkins server. Jenkins X is really a sub-project of Jenkins rather than a new product.

### Comments/Anecdotes
- From GitLab PMM
> - “Jenkins had to build a whole new separate project in order to work with Kubernetes. GitLab has natively adopted Kubernetes from the get-go.”
> - Jenkins X adoption is tiny. Most folks looking to go to Kubernetes will be on Jenkins proper, so the [Pinterest anecdote](/handbook/marketing/product-marketing/#deliver-value-faster) applies.
> - Although Jenkins X works with Kubernetes, it’s not a single application like GitLab. You still have to integrate to your PPM, SCM, security tools, etc. You have to manage permissions and access across all that which GitLab gives you out of the box, but Jenkins X does not ([value of a single app](/handbook/product/single-application/)).

### Resources
* [Jenkins X Website](https://jenkins-x.io/)

### Pricing
- No cost (and Open Source)
- But Total Cost of Ownership has cost (see [Pinterest anecdote](/handbook/marketing/product-marketing/#deliver-value-faster))
