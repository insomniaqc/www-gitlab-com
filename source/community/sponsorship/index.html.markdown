---
layout: markdown_page
title: "GitLab Diversity Sponsorship"
comments: false
sharing: true
suppress_header: true
---

## The Diversity Sponsorship Program is temporarily on Hold

Why is it on hold? We are working to improve and make the program better. 

Have ideas? Contribute here: https://gitlab.com/gitlab-com/marketing/general/issues/2842

---

![Community Sponsorship](/images/community/gitlab-growth.jpg)

In February 2016, [we announced a $500 USD grant](/2016/02/02/gitlab-diversity-sponsorship/) for events which promote diversity in technology.
Yep, all events which help increase diversity in technology automatically receive the grant.

If you intend to apply, please leave enough lead time (at least 4 weeks but
preferably more) for us to process your application.

[Apply for sponsorship](https://docs.google.com/forms/d/1FUm7DOc85yjplFj4zAIo3pqlGlbJR4c6AnHDHVv0k7Y/viewform)

Why is fostering diversity important?

As if the moral imperative and ethical rationale was not enough,
there are also practical advantages to fostering diversity.
These events help increase the potential pool of talent to work at GitLab.
Research has also proven that more diversity is better for business in almost
every aspect. ([McKinsey, 2015](http://www.mckinsey.com/insights/organization/why_diversity_matters))

### Eligibility (Non-Diversity Events)

If you're hosting an event which doesn't meet the description of an "event which promotes diversity in technology", we'd like to thank you for considering GitLab as your sponsor, unfortunately we don't respond to non-diversity requests for sponsoring. We identify sponsorship opportunities ourselves and reach out ourselves.

### Sponsored Events

You can take a look at the diversity events we sponsor on our [events page](/events/).

### Other sponsorships

- In 2017 we are sponsoring larger initatives such as [Ruby Together](https://rubytogether.org/), but would like to expand our reach into the community through a more localized sponsorship program.
- A special grant for the integration of GitLab into the Open Science Framework (OSF). Please read how you can help: [OSF grant.](/2015/09/03/a-grant-to-help-us-integrate-gitlab-with-open-source-osf/)
